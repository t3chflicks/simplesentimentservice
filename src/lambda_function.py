import json
from textblob import TextBlob

def handler(event, context):
    response = {
    "isBase64Encoded": False,
    "headers": {
        'access-control-allow-methods': 'OPTIONS, POST',
        'access-control-allow-origin': '*',
        'access-control-allow-headers': 'Content-Type, Access-Control-Allow-Headers'
        }
    }
    if event['httpMethod'] == "POST":
        try:
            body = json.loads(event["body"])
            text = body["text"]
            response["statusCode"] = 200 
            response["body"] = json.dumps({"SCORE": TextBlob(text).sentiment.polarity})
        except Exception as e:
            print("ERROR: ", e)
            response["statusCode"] = 400
            response["body"] = json.dumps({"ERROR": "INVALID TEXT PARAM"})
    return response