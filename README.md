# T3chFlicks - Simple Sentiment Service
An example text sentiment API created using 
AWS Lambda and Application Load Balancer. 
See the accompanying blog post [here](https://medium.com/p/b32b126bbddc).

### Usage
```
$ curl --location --request POST 'loadb-LoadB-R7RVQD09YC9O-1401336014.eu-west-1.elb.amazonaws.com' \
--header 'Content-Type: application/json' \
--data-raw '{
 "text": "This is great! I can't wait to implement it myself!"
}'
{
 "score": 1.0
}
```

### Architecture
![Architecture](./architecture.png)

### Setup
1. Package your Python Lambda as described here https://docs.aws.amazon.com/lambda/latest/dg/python-package.html
1. Create an S3 bucket
    * `aws s3 mb <bucket_name>`
1. Upload your package to the bucket
    * `aws s3 cp src/lambda.zip s3://<bucket_name>/lambda.zip`
1. Deploy VPC
    * `aws cloudformation create-stack --stack-name vpc --template-body file://aws/vpc.yml --capabilities CAPABILITY_NAMED_IAM`
    * tutorial for VPC can be found [here](insert_medium_link)
1. Deploy Service
    * `aws cloudformation create-stack --stack-name service --template-body file://aws/service.yml --capabilities CAPABILITY_NAMED_IAM`
    * update the template with your bucket name


### Stuck?
* Building your Lambda
    * [ERROR] Runtime.ImportModuleError: Unable to import module 'lambda_function': No module named 'regex._regex'
    * https://medium.com/i-like-big-data-and-i-cannot-lie/how-to-create-an-aws-lambda-python-3-6-deployment-package-using-docker-d0e847207dd6
    * https://docs.aws.amazon.com/lambda/latest/dg/python-package.html
    * https://hub.docker.com/r/lambci/lambda/

---

This project was created by [T3chFlicks](https://t3chflicks.org) a tech focused education and service company.

---


